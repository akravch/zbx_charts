Script for get charts from Zabbix and display in the browser window.

###Usage:
        1. Download file;
        2. Open in browser, supports IE6 and higher;
        3. Support URL arguments:

                [charts=chartid,period[;chartid,period]]
                [cols=colums_number(1-4)]
                [type=charts_type(chartsX.php)]

###Example:
```
file:///[location]/zbx_charts.html?charts=527,3600;525,3600;530,3600&cols=2&type=chart2.php
```

Arguments can appear in any order or may dismiss (will be use default values).


Some settings must be specified in the variables at the beginning of the script:
```
    var host = 'xxx.xxx.xxx.xxx';
    var user = 'admin';
    var pass = 'zabbix';

    var default_chart = '524';
    var default_period = '3600';
    var default_cols = '1';
    var default_type = 'chart2.php';

    var charts_url = 'http:\/\/' + host + '\/zabbix\/';
    var login_url = 'http:\/\/' + host + '\/zabbix\/index.php';

    //image size, which can return zabbix as response by unauthorized request
    var zabbix_not_authorized_img_height = 200;

    var start_delay = parseInt(Math.random() * 3000 + 2000); // 2-5 secs
    var update_delay = 3000;
```